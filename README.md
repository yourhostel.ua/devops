# DevOps



## Домашні роботи з DevOps

### hw1
- [ ] [Hello World](https://gitlab.com/yourhostel.ua/devops/-/blob/main/hw1/hello_world.sh)
- [ ] [User Input](https://gitlab.com/yourhostel.ua/devops/-/blob/main/hw1/user_input.sh)
- [ ] [Conditional Statements](https://gitlab.com/yourhostel.ua/devops/-/blob/main/hw1/conditional-statements.sh)
- [ ] [Looping](https://gitlab.com/yourhostel.ua/devops/-/blob/main/hw1/looping.sh)
- [ ] [File Operations](https://gitlab.com/yourhostel.ua/devops/-/blob/main/hw1/file_operations.sh)
- [ ] [String Manipulation.sh](https://gitlab.com/yourhostel.ua/devops/-/blob/main/hw1/string_manipulation.sh)
- [ ] [Command Line Arguments](https://gitlab.com/yourhostel.ua/devops/-/blob/main/hw1/сommand_line_arguments.sh)
- [ ] [Arrays](https://gitlab.com/yourhostel.ua/devops/-/blob/main/hw1/arrays.sh)
- [ ] [Error Handling](https://gitlab.com/yourhostel.ua/devops/-/blob/main/hw1/error-handling.sh)
- [ ] [Systemd Service](https://gitlab.com/yourhostel.ua/devops/-/blob/main/hw1/systemd_service.sh)

```
ssh-keygen
cat ~/.ssh/id_rsa.pub
```