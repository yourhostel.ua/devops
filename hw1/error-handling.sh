#!/bin/bash
# Скрипт, який намагається прочитати файл та обробляє помилки

read -p "Введіть назву файлу: " filename
if [ -f "$filename" ]; then
    cat "$filename"
else
    echo "Помилка: Файл $filename не знайдено."
fi