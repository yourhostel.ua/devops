#!/bin/bash
# Скрипт приймає назву файлу як аргумент командного рядка та виводить кількість рядків у цьому файлі

filename=$1
if [ -f "$filename" ]; then
    echo "Кількість рядків у файлі $filename: $(wc -l < $filename)"
else
    echo "Файл $filename не існує."
fi